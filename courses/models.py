from django.urls import reverse_lazy
from django.conf import settings
from django.db import models

from students.models import User

# Create your models here.
class Course(models.Model):
    name = models.CharField(max_length=200)
    students = models.ManyToManyField(User)

    def get_absolute_url(self):
        return reverse_lazy('course_detail', args=(self.id,))  # course_detail est le name de l'url en question donc à rajouter dans l'url

    def __str__(self):
        return self.name


class Section(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    title = models.CharField(max_length = 100)
    number = models.IntegerField()  # c'est pour le numéro de section
    text = models.TextField()

    class Meta:  # special param ORM Django
        unique_together = ('course', 'number',)  # cette combinaison doit être unique : seulement 1 numéro de section peut être relié qu'à 1 course

    def __str__(self):
        return self.title

    def get_test_url(self):
        return reverse_lazy('do_test', args=(self.id,))

    def get_absolute_url(self):
        return reverse_lazy('do_section', args=(self.id,))

    def get_next_section_url(self):
        next_section = Section.objects.get(number=self.number+1)
        return reverse_lazy('do_section', args=(next_section.id,))


class Question(models.Model):
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    text = models.CharField(max_length=1000)

    def __str__(self):
        return self.text


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.CharField(max_length = 1000)
    correct = models.BooleanField()

    def __str__(self):
        return self.text


class UserAnswer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    user=models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('question', 'user',)  # pour être sûr qu'1 seul User peut répondre à 1 question par 1 réponse

