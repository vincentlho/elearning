from django.core.exceptions import PermissionDenied, SuspiciousOperation
from django.urls import reverse
from django.db import transaction
from django.shortcuts import render, redirect
from django.views.generic import DetailView, CreateView, ListView

from django.http import HttpResponse, HttpResponseRedirect

from courses.forms import CourseForm
from courses.models import Course, Section, Question, UserAnswer
# Create your views here.


# def my_first_view(request):
#     return HttpResponse('Hello World')


def my_first_view(request, who):
    return render(request, 'courses/hello.html', {
        'who':who,
    })


# def course_detail(request, course_id):
#     course = Course.objects.get(id=course_id)
#     return render(request, 'courses/course_detail.html', {
#         'course':course,
#     })

class CourseDetailView(DetailView):
    model = Course

course_detail = CourseDetailView.as_view()


# def course_list(request):
#     #courses=Course.objects.all()
#     courses = Course.objects.prefetch_related('students')  # avec QuerySet pour préparer et pour performance et pour le cas M2M
#     return render(request, 'courses/course_list.html', {
#         'courses':courses,
#     })


class CourseListView(ListView):
    model = Course
    queryset = Course.objects.prefetch_related('students')

course_list = CourseListView.as_view()


# def course_add(request):
#     if request.POST:
#         form = CourseForm(request.POST)
#         if form.is_valid():
#             new_course = form.save()
#             return HttpResponseRedirect(new_course.get_absolute_url())
#
#     else:
#         form=CourseForm()
#     return render(request, 'courses/course_form.html',{
#         'form': form,
#     })


class CourseAddView(CreateView):
    model = Course
    fields = '__all__'  # secure souci : best practice est de spécifier chaque champ que tu veux afficher

course_add = CourseAddView.as_view()


def do_section(request, section_id):
    section = Section.objects.get(id=section_id)
    return render(request, 'courses/do_section.html',{
        'section':section,
    })

def do_test(request, section_id):
    #  verif si l'user est authentifié
    if not request.user.is_authenticated:
        raise PermissionDenied
    # on récup la section spécifiée dans l'url
    section = Section.objects.get(id=section_id)
    if request.method == 'POST':
        #  pour rendre l'action atomique dans la BDD sinon rien se sera fait ni modifié
        with transaction.atomic():
            # on efface toutes les réponse déjà faites à cette question dans le cas où le user ait déjà répondu à cette question
            UserAnswer.objects.filter(user=request.user,
                                      question__section = section).delete()
            #  on itére sur la réponse POST et le rendre exploitable (normalement, on utilise Django Form)
            for key, value in request.POST.items():
                #  pour vérifier si on est bien sécurisé en CSRF
                if key == 'csrfmiddlewaretoken':
                    continue
                # {'question-1':'2'} c'est ce que l'on recoît en réponse de la requête
                question_id = key.split('-')[1]  # on souhaite seulement l'ID de la question sur la clef du dictionnaire
                question = Question.objects.get(id=question_id)  # on récupère la question selon cette ID
                answer_id = int(request.POST.get(key))  # on récupère aussi l'ID de la réponse à la question qui est la valeur value du dictionnaire récupéré
                #  test sécurité si la réponse est bien présente dans la liste possible des réponses à la question
                if answer_id not in question.answer_set.values_list('id', flat=True):
                    raise SuspiciousOperation('Answer is not valid for this question')
                # création d'un nouveau UserAnswer
                user_answer = UserAnswer.objects.create(
                    user=request.user,
                    question = question,
                    answer_id=answer_id,
                )
        return redirect(reverse('show_results', args=(section_id,)))
    return render(request, 'courses/do_test.html', {
        'section': section,
    })


def calculate_score(user, section):
    questions = Question.objects.filter(section=section)
    correct_answers = UserAnswer.objects.filter(
        user=user,
        question__section = section,
        answer__correct = True  # c'est ici que l'on filtre le correct answers lié au USER : on ne veut que les Answers qui ont le booléan correct à True, différent du UserAnswer
    )
    return (correct_answers.count() / questions.count()) * 100


def show_results(request, section_id):
    if not request.user.is_authenticated:
        raise PermissionDenied
    section = Section.objects.get(id=section_id)
    return render(request, 'courses/show_results.html',{
        'section': section,
        'score': calculate_score(request.user, section)
    })